from typing import (
    Dict,
    List,
    Literal,
    Optional,
    Tuple,
    TypedDict,
    Union,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)

Movement = Literal["L", "R"]
Action = Literal["write", Movement]
Instruction = Union[Movement, Dict[Action, str]]
TransitionTable = Dict[str, Dict[str, Instruction]]
TuringMachine = TypedDict(
    "TuringMachine",
    {
        "blank": str,
        "start state": str,
        "final states": List[str],
        "table": TransitionTable,
    },
)

class TuringMC(TypedDict):
    # Entry of a Turing machine step for logging purposes.

    state: str
    reading: str
    position: int
    memory: str
    transition: Instruction


def run_turing_machine(
    machine: TuringMachine,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List, bool]:


    transTable = machine["table"]
    state = machine["start state"]
    finalStates = machine["final states"]
    blank = machine["blank"]

    # Initialize tape with input
    mem: list[str] = list(input_)
    history: List[HistoryEntry] = []
    pos = 0
    stepsTaken = 0

    while state not in finalStates:
        if steps is not None and stepsTaken >= steps:
            # If maximum number of steps reached: halt
            break

        if pos < 0:
            # Shift tape to the right and insert blank symbol
            mem.insert(0, blank)
            pos = 0
        elif pos >= len(mem):
            # Add blank symbol to EOTape
            mem.append(blank)

        symbol = mem[pos]

        if state not in transTable or symbol not in transTable[state]:
            # If missing transition: halt
            break

        instruction = transTable[state][symbol]

        history.append(
            TuringMC(
                state=state,
                reading=symbol,
                position=pos,
                memory="".join(mem),
                transition=instruction,
            )
        )

        if instruction == "L":
            pos -= 1
        elif instruction == "R":
            pos += 1
        else:
            if "write" in instruction:
                mem[pos] = instruction["write"]

            if "L" in instruction:
                pos -= 1
                state = instruction["L"]
            elif "R" in instruction:
                pos += 1
                state = instruction["R"]
            else:
                #If missing movement instruction: halt
                pos += 0
                break

        stepsTaken += 1

    return "".join(mem).strip(blank), history, state in finalStates


del poetry_version, Dict, List, Literal, Optional, Tuple, TypedDict, Union
